#This is a small test case.

##Outputs

    TestApp::TestApp(int&, char**) 
    virtual Session::~Session() Session(0x103004be0, name = "Session")
    virtual BaseObject::~BaseObject() BaseObject(0x103004be0, name = "Session") 
    void TestApp::onDestroyed(QObject*) (BaseObject(0x103004c00, name = "Child") )  
    virtual BaseObject::~BaseObject() BaseObject(0x103004c00, name = "Child") 
    virtual TestApp::~TestApp() 

Which seems fine.

##However

If I do a small change, and add 

    diff --git a/Test.h b/Test.h
    index 51290d7..e1dc6c3 100644
    --- a/Test.h
    +++ b/Test.h
    @@ -32,6 +32,8 @@ public:
             m_child->deleteLater();
         }
    
    +    bool event(QEvent *e){ e->ignore(); return false; }
    +
     private:
         Session() : BaseObject()
         {

to Session (doesnt matter if I accept or not), The output becomes just:

    TestApp::TestApp(int&, char**)

Meaning, the qobjects never gets deleted... Why?
