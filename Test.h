
#ifndef __Test__
#define __Test__

#include <QObject>
#include <QEvent>
#include <QDebug>

class BaseObject : public QObject
{
    Q_OBJECT
public:
    BaseObject(QObject *parent = 0) : QObject(parent) {}
    ~BaseObject(){ qDebug() << Q_FUNC_INFO << this; }
    bool event(QEvent *e)
    {
        if(e->type() == QEvent::DeferredDelete)
        {
            return QObject::event(e);
        }

        e->ignore();
        return false;
    }
private:

};

class Session : public BaseObject
{
    Q_OBJECT
public:
    static Session* instance()
    {
        if(!s_instance)
            s_instance = new Session();
        return s_instance;
    }
    ~Session()
    {
        qDebug() << Q_FUNC_INFO << this;
        m_child->deleteLater();
    }

    bool event(QEvent *e)
    {
        if(!BaseObject::event(e))
        {
            qDebug() << "This I can accept:" << e->type();
            e->accept();
            return true;
        }

        qDebug() << "This i dont want:" << e->type();
        e->ignore();
        return false;
    }
private:
    Session() : BaseObject()
    {
        setObjectName("Session");
        m_child = new BaseObject(this);
        m_child->setObjectName("Child");
    }

    static Session *s_instance;
    BaseObject *m_child;
};


#endif


