#include "TestApp.h"
#include <QTimer>
#include <QDebug>

Session *Session::s_instance = 0;

TestApp::TestApp( int& argc, char** argv )
    : QCoreApplication( argc, argv )
    , m_sess(Session::instance())
{
    qDebug() << Q_FUNC_INFO;

    connect(this, SIGNAL(destroyed(QObject*)), SLOT(onDestroyed(QObject*)));
    connect(m_sess, SIGNAL(destroyed(QObject*)), SLOT(onDestroyed(QObject*)));

    m_sess->deleteLater();
}

void TestApp::onDestroyed(QObject* obj)
{
    qDebug() << Q_FUNC_INFO << obj->children();
    quit();
}

TestApp::~TestApp()
{
    qDebug() << Q_FUNC_INFO;
}
