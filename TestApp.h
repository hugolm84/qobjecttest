
#ifndef __TestApp__
#define __TestApp__

#include <QCoreApplication>
#include <Test.h>

class TestApp : public QCoreApplication
{
    Q_OBJECT
public:
    TestApp( int& argc, char** argv );
    ~TestApp();
public slots:
    void onDestroyed(QObject*);
private:
    Session *m_sess;
};
#endif


