#include <QCoreApplication>
#include <TestApp.h>

int main(int argc, char *argv[])
{
    TestApp a(argc, argv);
    return a.exec();
}
